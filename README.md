**# IntelliBox by IntelliSrc**

Cross-platform application for labelling object bounding boxes on images inspired by [BBox](https://github.com/puzzledqs/BBox-Label-Tool) developed in Java.

![IntelliBox labelling object tool: screenshot](./images/screenshot.jpg)

Since we have tried different labelling tools available on the Web and experienced different slight difficulties (some projects have
been abandoned or are tailored for specific frameworks), we decided to do our own open source labelling tool. We are expecting the community to help us
improve this project. Our goal is to create a single tool for any **deeplearning** framework!

This labelling tool is capable of saving different annotations formats at the same time. So it's very convenient for the user
to choose the annotation needed for configuration.


# Supported Formats:

- JSON (general use)
- XML (PascalVoc)
- TXT (Bbox, Coco) user don't need of having [`convert.py`](https://github.com/Guanghan/darknet/blob/master/scripts/convert.py) script because the conversion is done automatically.

# TOOLS NEEDED FOR DEVELOPMENT:
    JDK >= 8
    JavaFX
    IntelliJ IDEA

As the installation of these tools its generally easy, we are not going to cover it here.

# USAGE:
    1. Download the Jar file (coming soon)
    2. Execute: `java -jar intellibox.jar`

# SHORTCUT KEYS
    1. Click the desired box's dimension from the list on the top left side of the window and start moving the box by pressing the ASWD key:
        - A = Left
        - S = Down
        - W = Up
        - D = Right
       Press Shift+A, Shift+S, Shift+W, or Shift+D for faster movement of the box.
    2. Press SPACE to save and navigate to the next image.
    3. Press BACKSPACE to go back to the previous image.
    4. Press DELETE key to delete the image (note that this will not delete the created annotations, working on that).
    5. Press C key to open the Annotation/Classes list window and press the shortcut key for chosen class that you normally see when the window showed up.
    6. Edit the box dimension from the Annotation/Classes list window if you want to make an adjustment with the box's dimensions.
    7. Press Q key to hide/unhide the bbox label and to know where it was place in the image