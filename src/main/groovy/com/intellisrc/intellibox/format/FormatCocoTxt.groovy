package com.intellisrc.intellibox.format

import com.intellisrc.core.Config
import com.intellisrc.intellibox.util.Annotation
import com.intellisrc.intellibox.util.Class
import groovy.transform.CompileStatic
import javafx.scene.image.Image
import javafx.scene.shape.Rectangle

/**
 * @since 18/08/30.
 */
@CompileStatic
class FormatCocoTxt extends FormatBase {
    FormatCocoTxt() {
        enabled = Config.getBool("format.coco.enabled") ?: true
    }

    @Override
    String getFileExtension() {
        return Config.get("format.coco.ext") ?: "txt"
    }

    @Override
    String getPath() {
        return Config.get("format.coco.path") ?: "Annotations/coco"
    }

    @Override
    boolean export(Annotation annotation) {
        File expFile = getExportFile(annotation)
        expFile.text = ""
        annotation.boxes.each {
            Annotation.Box box ->
                double xmin = box.rect.x
                double xmax = box.rect.x + box.rect.width
                double ymin = box.rect.y
                double ymax = box.rect.y + box.rect.height
                double dw = 1 / annotation.image.width
                double dh = 1 / annotation.image.height
                double x = (xmin + xmax) / 2d //center point
                double y = (ymin + ymax) / 2d //center point
                double w = box.rect.width
                double h = box.rect.height
                String out = [box.cls.id, x * dw, y * dh, w * dw, h * dh].join(" ")
                expFile << out + "\n"
        }
        new File(expFile.parent + File.separator + "coco.names").text = Class.list.collect { it.name }.join("\n")
        return expFile.exists() && expFile.canWrite() && expFile.text
    }

    @Override
    Annotation getAnnotation(File imgFile) {
        Annotation annotation = null
        if(imgFile.exists()) {
            annotation = new Annotation(imgFile)
            File expFile = getExportFile(annotation)
            if(expFile.exists() &&! expFile.text.empty) {
                boolean hasNames = false
                File names = new File(expFile.parent + File.separator + "coco.names")
                if(names.exists()) {
                    names.eachLine {
                        Class.addClass(it)
                    }
                    hasNames = true
                }
                expFile.eachLine {
                    String row ->
                        List<String> parts = row.split(" ").toList()
                        int id = parts[0] as int
                        Class cls = hasNames ? Class.list[id] : new Class("class-${id}")
                        double xdw = parts[1] as double
                        double ydh = parts[2] as double
                        double wdw = parts[3] as double
                        double hdh = parts[4] as double
                        Image img = annotation.image
                        double width = img.width
                        double height = img.height
                        double dw = 1 / width
                        double dh = 1 / height
                        double w = wdw / dw
                        double h = hdh / dh
                        double x = (xdw / dw) - w/2d
                        double y = (ydh / dh) - h/2d
                        annotation.boxes << new Annotation.Box(
                                cls: cls,
                                rect: new Rectangle(x, y, w, h)
                        )
                }
            }
        }
        return annotation
    }
}
