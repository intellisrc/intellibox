package com.intellisrc.intellibox.format

import com.intellisrc.core.Config
import com.intellisrc.intellibox.util.Annotation
import com.intellisrc.intellibox.util.Class
import com.intellisrc.web.JSON
import groovy.transform.CompileStatic
import javafx.scene.shape.Rectangle

@CompileStatic
class FormatJson extends FormatBase {
    FormatJson() {
        enabled = Config.getBool("format.json.enabled") ?: true
    }

    @Override
    String getFileExtension() {
        return Config.get("format.json.ext") ?: "json"
    }

    @Override
    String getPath() {
        return Config.get("format.json.ext") ?: "Annotations/json"
    }

    @Override
    boolean export(Annotation annotation) {
        File expFile = getExportFile(annotation)
        expFile.text = JSON.encode(annotation.toMap(), true)
        return expFile.exists() && expFile.canWrite() && expFile.text
    }

    @Override
    Annotation getAnnotation(File imgFile) {
        Annotation annotation = null
        if(imgFile.exists()) {
            annotation = new Annotation(imgFile)
            File expFile = getExportFile(annotation)
            if(expFile.exists() &&! expFile.text.empty) {
                Map json = JSON.decode(expFile.text).toMap()
                Map<String,String> clss = json["classes"] as Map<String,String>
                clss.keySet().each {
                    Class.addClass(clss[it], it as int)
                }
                json["boxes"].each {
                    Map map ->
                    annotation.boxes << new Annotation.Box(
                        cls : Class.fromId(map["id"] as int),
                        rect: new Rectangle(
                                map["x"] as double,
                                map["y"] as double,
                                map["w"] as int,
                                map["h"] as int
                        )
                    )
                }
            }
        }
        return annotation
    }
}
