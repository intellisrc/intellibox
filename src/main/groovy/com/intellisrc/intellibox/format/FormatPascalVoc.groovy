package com.intellisrc.intellibox.format

import com.intellisrc.core.Config
import com.intellisrc.intellibox.util.Annotation
import com.intellisrc.intellibox.util.Class
import groovy.transform.CompileStatic
import groovy.util.slurpersupport.GPathResult
import javafx.scene.shape.Rectangle

/**
 * Export in Pascal VOC format
 * @since 18/08/30.
 */
@CompileStatic
class FormatPascalVoc extends FormatBase {
    int depth = Config.getInt("format.voc.depth") ?: 3
    int segmented = Config.getInt("format.voc.segmented") ?: 0
    int truncated = Config.getInt("format.voc.truncated") ?: 0
    int difficult = Config.getInt("format.voc.difficult") ?: 0
    String database = Config.get("format.voc.database") ?: "Unknown"
    String pose = Config.get("format.voc.pose") ?: "Unspecified"

    FormatPascalVoc() {
        enabled = Config.getBool("format.voc.enabled") ?: true
    }

    @Override
    String getFileExtension() {
        return Config.get("format.voc.ext") ?: "xml"
    }

    @Override
    String getPath() {
        return Config.get("format.voc.path") ?: "Annotations/xml"
    }

    @Override
    boolean export(Annotation annotation) {
        File expFile = getExportFile(annotation)
        expFile.text = ""
        String boxesXML = ""
        annotation.boxes.each {
            Annotation.Box box ->
                boxesXML += """
<object>
    <name>${box.cls.name}</name>
    <pose>$pose</pose>
    <truncated>$truncated</truncated>
    <difficult>$difficult</difficult>
    <bndbox>
        <xmin>${box.rect.x as int}</xmin>
        <ymin>${box.rect.y as int}</ymin>
        <xmax>${(box.rect.x + box.rect.width) as int}</xmax>
        <ymax>${(box.rect.y + box.rect.height) as int}</ymax>
    </bndbox>
</object>
"""
        }
        expFile << """<annotation>
<folder>${annotation.imageFile.parent}</folder>
<filename>${annotation.imageFile.name}</filename>
<path>${annotation.imageFile.absolutePath}</path>
<source><database>$database</database></source>
<size>
    <width>${annotation.image.width as int}</width>
    <height>${annotation.image.height as int}</height>
    <depth>$depth</depth>
</size>
<segmented>$segmented</segmented>
$boxesXML
</annotation>"""
        return expFile.exists() && expFile.canWrite() && expFile.text
    }

    @Override
    Annotation getAnnotation(File imgFile) {
        Annotation annotation = null
        if(imgFile.exists()) {
            annotation = new Annotation(imgFile)
            File expFile = getExportFile(annotation)
            if(expFile.exists() &&! expFile.text.empty) {
                GPathResult root = new XmlSlurper().parse(expFile)
                root["object"].each {
                    GPathResult obj ->
                        Class cls = new Class(obj["name"].toString())
                        annotation.boxes << new Annotation.Box(
                                cls: cls,
                                rect: new Rectangle(
                                        obj["bndbox"]["xmin"].toString() as int,
                                        obj["bndbox"]["ymin"].toString() as int,
                                        ((obj["bndbox"]["xmax"].toString() as int) - (obj["bndbox"]["xmin"].toString() as int)) as int,
                                        ((obj["bndbox"]["ymax"].toString() as int) - (obj["bndbox"]["ymin"].toString() as int)) as int
                                )
                        )
                }
            }
        }
        return annotation
    }

}
