package com.intellisrc.intellibox.format

import com.intellisrc.core.Config
import com.intellisrc.intellibox.util.Annotation
import com.intellisrc.intellibox.util.Class
import groovy.transform.CompileStatic
import javafx.scene.shape.Rectangle

@CompileStatic
class FormatBBoxTxt extends FormatBase {
    FormatBBoxTxt() {
        enabled = Config.getBool("format.bbox.enabled") ?: true
    }

    @Override
    String getFileExtension() {
        return Config.get("format.bbox.ext") ?: "txt"
    }

    @Override
    String getPath() {
        return Config.get("format.bbox.path") ?: "Annotations/bbox"
    }

    @Override
    boolean export(Annotation annotation) {
        File expFile = getExportFile(annotation)
        expFile.text = annotation.boxes.size() + "\n"
        annotation.boxes.each {
            Annotation.Box box ->
                expFile << [
                    box.rect.x,
                    box.rect.y,
                    (box.rect.x + box.rect.width),
                    (box.rect.y + box.rect.height),
                    box.cls.id,
                ].join(" ") + "\n"
        }
        new File(expFile.parent + File.separator + "class.names").text = Class.list.collect { it.name }.join("\n")
        return expFile.exists() && expFile.canWrite() && expFile.text
    }

    @Override
    Annotation getAnnotation(File imgFile) {
        Annotation annotation = null
        if(imgFile.exists()) {
            annotation = new Annotation(imgFile)
            File expFile = getExportFile(annotation)
            if(expFile.exists() &&! expFile.text.empty) {
                boolean hasNames = false
                File names = new File(expFile.parent + File.separator + "class.names")
                if(names.exists()) {
                    names.eachLine {
                        Class.addClass(it)
                    }
                    hasNames = true
                }
                LinkedList<String> lines = expFile.readLines() as LinkedList<String>
                lines.pollFirst()
                lines.each {
                    String row ->
                        List<String> parts = row.split(" ").toList()
                        int id = parts.size() > 4 ? (parts[4] as int) : 0
                        Class cls = hasNames ? Class.list[id] : new Class("class-${id}")
                        annotation.boxes << new Annotation.Box(
                                cls: cls,
                                rect: new Rectangle(
                                        parts[0] as double,
                                        parts[1] as double,
                                        (parts[2] - parts[0]) as double,
                                        (parts[3] - parts[1]) as double
                                )
                        )
                }
            }
        }
        return annotation
    }
}
