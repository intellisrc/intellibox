package com.intellisrc.intellibox.format

import com.intellisrc.core.Log
import com.intellisrc.intellibox.util.Annotation
import groovy.transform.CompileStatic

/**
 * @since 18/09/05.
 */
@CompileStatic
class Formats {
    static final List<FormatBase> list = []
    /**
     * Export to a directory all formats
     * @param dir
     */
    static export(final Annotation annotation) {
        boolean success = true
        list.each {
            FormatBase format ->
                if(format.enabled) {
                    createIfNotExists(annotation.imageFile.parent + File.separator + format.path)
                    if(!format.export(annotation)) {
                        Log.w("Format ${format.class.simpleName} failed.")
                        return false
                    }
                }
        }
        return success
    }
    /**
     * load annotation from the first format available
     * @param file
     */
    static Annotation getAnnotation(final File file) {
        Annotation annotation = null
        list.any {
            FormatBase format ->
                if(format.enabled) {
                    annotation = format.getAnnotation(file)
                    if(annotation?.boxes?.empty) {
                        annotation = null
                    }
                }
                return (annotation != null)
        }
        return annotation
    }
    /**
     * If directory doesn't exists, create it
     * @param path
     * @return
     */
    private static boolean createIfNotExists(String path) {
        File dir = new File(path)
        if (!dir.exists()) {
            if (dir.mkdirs()) {
                Log.i("Directory was created!")
            } else {
                Log.w("Failed to create directory!")
            }
        }
        return dir.exists()
    }
}
