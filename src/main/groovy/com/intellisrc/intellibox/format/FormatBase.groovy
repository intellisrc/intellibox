package com.intellisrc.intellibox.format

import com.intellisrc.core.Log
import com.intellisrc.intellibox.util.Annotation
import groovy.transform.CompileStatic

/**
 * @since 18/08/30.
 */
@CompileStatic
abstract class FormatBase {
    boolean enabled = true
    /**
     * Specify the label for user in the selection
     * @return
     */
    abstract String getFileExtension()
    /**
     * Path relative to selected directory
     * @return
     */
    abstract String getPath()
    /**
     * Export annotation
     * @param annotation
     * @return
     */
    abstract boolean export(Annotation annotation)
    abstract Annotation getAnnotation(final File file)
    /**
     * Return the export file according to the format
     * @param annotation
     * @return
     */
    File getExportFile(Annotation annotation) {
        File expFile = new File(annotation.imageFile.parent + File.separator + (path ? path + File.separator : '') + annotation.imageFile.name.replaceFirst(/\.\w+$/, "." + fileExtension))
        Log.v("Format file: ${expFile.absolutePath}")
        return expFile
    }
}
