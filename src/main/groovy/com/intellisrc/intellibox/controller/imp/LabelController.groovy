package com.intellisrc.intellibox.controller.imp

import com.intellisrc.core.Config
import com.intellisrc.core.Log
import com.intellisrc.core.SysInfo
import com.intellisrc.intellibox.controller.AbstractController
import com.intellisrc.intellibox.handler.ViewHandler
import com.intellisrc.intellibox.util.Annotation
import com.intellisrc.intellibox.util.BoxProperties
import com.intellisrc.intellibox.util.Class
import com.intellisrc.intellibox.util.Classes
import com.intellisrc.intellibox.util.MoveRect
import com.intellisrc.intellibox.format.FormatBBoxTxt
import com.intellisrc.intellibox.format.FormatCocoTxt
import com.intellisrc.intellibox.format.FormatJson
import com.intellisrc.intellibox.format.FormatPascalVoc
import com.intellisrc.intellibox.format.Formats
import com.intellisrc.intellibox.util.Paths
import com.intellisrc.intellibox.view.dialog.ClassesPane
import javafx.application.Platform
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.concurrent.Task
import javafx.concurrent.WorkerStateEvent
import javafx.event.ActionEvent
import javafx.geometry.Insets
import javafx.scene.Cursor
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.effect.Light
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.scene.shape.Line
import javafx.scene.shape.Rectangle
import javafx.stage.DirectoryChooser
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.fxml.FXML
import groovy.transform.CompileStatic
import javafx.util.Callback

import static com.intellisrc.intellibox.util.Paths.ICON_PATH
import javax.imageio.ImageIO

@CompileStatic
class LabelController extends AbstractController {
    @FXML
    BorderPane mainBorderPane
    // buttons
    @FXML
    private Button btnNext
    @FXML
    private Button btnClear
    @FXML
    private Button btnSearch
    @FXML
    private Button btnDelete
    @FXML
    private Button btnPrevious
    @FXML
    private Button btnOpenImageDir
    @FXML
    private Button btnOpenAnnotationsDir
    @FXML
    private Button btnProcess
    @FXML
    private Button btnSettings
    // textfields
    @FXML
    private TextField tfSearch
    // table view
    @FXML
    private TableView<BoxProperties> tvBoundingBoxes
    // table columns
    @FXML
    private TableColumn<BoxProperties, String> tcClassName
    @FXML
    private TableColumn<BoxProperties, Integer> tcHeight
    @FXML
    private TableColumn<BoxProperties, Integer> tcWidth
    // listviews
    @FXML
    private ListView lvImageView    //Thumbnail view on the left
    // groups
    @FXML
    private Group grpImageView      //Group for main image. Includes boxes and events
    // vbox
    @FXML
    private VBox mainVBox
    // imageviews
    @FXML
    private ImageView mainImageView     //Main image
    // scrollpane
    @FXML
    private ScrollPane spMiddleBorderPane

    // javafx
    private Group groupForRectangles = new Group()
    private Group groupForGuides = new Group()
    private DirectoryChooser directoryChooser
    private Rectangle rectangleObject = null

    // object
    private Annotation annotation
    private Color guideColor = Config.hasKey("guides.color") ? Color.web(Config.get("guides.color")) : Color.MAGENTA
    private Color tempBoxColor = Config.hasKey("box.default.color") ? Color.web(Config.get("box.default.color")) : Color.MAGENTA
    private ResourceBundle bundle

    // int
    private int index = -1
    int counter = 0
    private int fileMax = Config.getInt("files.max")

    // double
    private double startingPointX, startingPointY
    private double currentEndingPointX, currentEndingPointY

    // boolean
    private boolean isRectangleBeingDrawn = false
    private boolean isUserLocatedAnAnnotationDirectory = false

    // list
    private List<File> listOfImages = []
    private List<ImageView> listOfImageView = []
    private Map<String, String> colors = new HashMap<>()
    private List<Label> listOfLabels = []

    LabelController(ViewHandler viewHandler) {
        super(viewHandler)
        Log.i("Loading images from directory: %s ...", Paths.imageDirectory.absolutePath)
    }

    /**
     * search image
     * @param file
     * @param imageName
     */
    private void searchFile(File file, String imageName = "") {
        //do you have permission to read this directory?
        ObservableList<File> filter = FXCollections.observableArrayList()
        if (file.canRead()) {
            file.listFiles().findAll {
                File fSearch ->
                    if(fSearch.name =~ /(?i)\.(jpe?g|png|gif|tiff)$/ && fSearch.name.contains(imageName)) {
                        filter.add(fSearch)
                    }
            }
            displayImageFromSearch(filter)
        } else {
            showAlert("Permission denied", "Permission to open directory denied!", "Denied!", -1, Alert.AlertType.ERROR)
        }
    }

    /**
     * user's own choice of directory
     * @param directoryChooser
     */
    private static void configuringDirectoryChooser(DirectoryChooser directoryChooser) {
        // Set title for DirectoryChooser
        directoryChooser.setTitle("Image Directory")
        // Set Initial Directory
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")))
    }

    /**
     * display images
     * @param directory
     */
    private void displayImageToListView(final File directory) {
        if (directory == null) {
            Log.v("Directory was not selected.")
        } else {
            lvImageView.items.clear()
            listOfImageView.clear()
            listOfImages.clear()
            if (directory.isDirectory()) {
                int count = 0
                Paths.imageDirectory = directory
                Paths.imageDirectory.eachFileAsync {
                    File file ->
                        if (!file.isDirectory() && file.name =~ /(?i)\.(jpe?g|png|gif|tiff)$/) {
                            //Add image to list
                            if ((!fileMax || count < fileMax) && ImageIO.read(file) != null) {
                                count++
                                listOfImages.add(file)
                            }
                        }
                }
                if (listOfImages.size() > 0) {
                    listOfImages = listOfImages.sort()
                    runTask(listOfImages.size())
                    Platform.runLater({
                        listOfImages.each {
                            File file ->
                                lvImageView.items.add(createImageView(file))
                        }
                        if (lvImageView.items.size() > 0)
                            tfSearch.disable = false
                    })
                }
            } else {
                lvImageView.items.add(createImageView(directory))
                loadImage(directory)
            }
        }
    }

    /**
     * display images that contains similar properties
     * @param filter
     */
    private void displayImageFromSearch(ObservableList<File> filter) {
        lvImageView.items.clear()
        listOfImageView.clear()
        listOfImages.clear()
        filter.each {
            File file ->
                listOfImages.add(file)
        }
        if (listOfImages.size() > 0) {
            listOfImages.each {
                File file ->
                    lvImageView.items.add(createImageView(file))
            }
        }
    }

    /**
     * ImageView to be displayed on VBox controller
     * @param imageFile
     * @return
     */
    private ImageView createImageView(final File imageFile) {
        mainImageView.image = null
        tvBoundingBoxes.items.clear()
        groupForRectangles.children.clear()
        double width = 150
        ImageView imageView = null
        try {
            final Image imageSized = new Image(new FileInputStream(imageFile), width, 0, true, true)
            Log.v("Size: ${imageSized.width} x ${imageSized.height}")
            imageView = new ImageView(imageSized)
            imageView.fitWidth = width
        } catch (FileNotFoundException ex) {
            Log.e("File not found: ", ex)
        }
        return imageView
    }

    private void loadImage(File imageFile) {
        assert imageFile != null
        mainImageView.image = new Image(new FileInputStream(imageFile))
        title = imageFile.name + " ( " + (mainImageView.image.width as int) + " x " + (mainImageView.image.height as int) + " )"

        annotation = Formats.getAnnotation(imageFile) ?: new Annotation(imageFile, mainImageView.image)
        tvBoundingBoxes.disable = false
        drawBoxes()

        final Rectangle selection = new Rectangle()
        final Light.Point anchor = new Light.Point()

        final Line horizontalLine = new Line(spMiddleBorderPane.hvalue, 0,
                annotation.image.width, 0)
        final Line verticalLine = new Line(0, spMiddleBorderPane.vvalue, 0,
                annotation.image.height)
        verticalLine.stroke = guideColor
        verticalLine.strokeDashArray.addAll([6d, 6d])
        horizontalLine.stroke = guideColor
        horizontalLine.strokeDashArray.addAll([6d, 6d])
        groupForGuides.with {
            children.clear()
            children.add(horizontalLine)
            children.add(verticalLine)
            disable = true
        }
        mainImageView.onMouseEntered = {
            MouseEvent mouseEvent ->
                groupForGuides.disable = false
                mainImageView.cursor = Cursor.CROSSHAIR
                groupForGuides.cursor = Cursor.CROSSHAIR
        }

        grpImageView.with {
            onMouseClicked = {
                MouseEvent mouseEvent ->
                    if (mouseEvent.button == MouseButton.PRIMARY) {
                        drawBox(mouseEvent.getX(), mouseEvent.getY())
                    } else if (mouseEvent.getButton() == MouseButton.SECONDARY){
                        cancelDrawingBox()
                    }
            }
            onMouseExited = {
                MouseEvent mouseEvent ->
                    stopDrawingBox()
            }
            onMouseMoved = {
                MouseEvent mouseEvent ->
                    // don't change getX(), getY() as x or y for groovy because this doesn't work with Mac users
                    double x = mouseEvent.getX()
                    double y = mouseEvent.getY()

                    currentEndingPointX = x
                    currentEndingPointY = y

                    selection.width = Math.abs(x - anchor.getX())
                    selection.height = Math.abs(y - anchor.getY())
                    selection.x = Math.min(anchor.getX(), x)
                    selection.y = Math.min(anchor.getY(), y)

                    if (!groupForGuides.children.empty) {
                        horizontalLine.startY = y
                        horizontalLine.endY = y
                        verticalLine.startX = x
                        verticalLine.endX = x
                    }

                    if (isRectangleBeingDrawn) {
                        if (groupForRectangles.children.size() > 0) {
                            int last = groupForRectangles.children.size() - 1
                            Rectangle rectangle = groupForRectangles.children.get(last) as Rectangle
                            adjustRectangleCoordinates(startingPointX, startingPointY, currentEndingPointX, currentEndingPointY, rectangle)
                        }
                    }
            }
        }
        mainImageView.requestFocus()
    }

    /**
     * Automatically starts or stop drawing box
     * @param x
     * @param y
     */
    private void drawBox(double x, double y) {
        if (!isRectangleBeingDrawn) {
            startingPointX = x
            startingPointY = y
            startDrawingBox()
        } else {
            stopDrawingBox()
        }
    }

    /**
     * Starts drawing box
     */
    private void startDrawingBox() {
        if(!isRectangleBeingDrawn) {
            Rectangle rectangle = new Rectangle()
            rectangle.setFill(Color.TRANSPARENT)
            // A non-finished rectangle has always the same color.
            rectangle.setStroke(tempBoxColor)
            groupForRectangles.getChildren().add(rectangle)
            isRectangleBeingDrawn = true
        }
    }

    /**
     * Stops drawing box
     */
    private void stopDrawingBox() {
        if (isRectangleBeingDrawn) {
            // Now the drawing of the new rectangle is finished.
            // Let's set the final color for the rectangle.
            int last = groupForRectangles.children.size() - 1
            Rectangle rectangle = groupForRectangles.children.get(last) as Rectangle

            rectangleObject = rectangle
            isRectangleBeingDrawn = false

            showClassesDialog()
        }
    }

    private void cancelDrawingBox() {
        if (isRectangleBeingDrawn) {
            removeBoundingBox(groupForRectangles.getChildren().size() - 2)
            isRectangleBeingDrawn = false
        }
    }

    /**
     * This will adjusts the coordinates so that the rectangle
     * is shown "in a correct way" in relation to the mouse movement.
     * @param startingPointX
     * @param startingPointY
     * @param endingPointX
     * @param endingPointY
     * @param rectangle
     */
    private static void adjustRectangleCoordinates(double startingPointX, double startingPointY, double endingPointX, double endingPointY, Rectangle rectangle) {
        rectangle.setX(startingPointX)
        rectangle.setY(startingPointY)
        rectangle.setWidth(endingPointX - startingPointX)
        rectangle.setHeight(endingPointY - startingPointY)

        if (rectangle.getWidth() < 0) {
            rectangle.setWidth(-rectangle.getWidth())
            rectangle.setX(rectangle.getX() - rectangle.getWidth()) // don't change getX(), getY() as x or y for groovy because this doesn't work with Mac users
        }

        if (rectangle.getHeight() < 0) {
            rectangle.setHeight(-rectangle.getHeight())
            rectangle.setY(rectangle.getY() - rectangle.getHeight())
        }
    }

    private boolean saveAnnotations() {
        boolean saved = Formats.export(annotation)
        clearDrawing()
        return saved
    }

    // for future reference on this : http://www.java2s.com/Code/Java/JavaFX/Setstagexandyaccordingtoscreensize.htm
    private void showClassesDialog() {
        Classes.ObjClass oclass
        Dialog dialog = new Dialog()
        ClassesPane cp = new ClassesPane(bundle)
        dialog.with {
            initOwner(stage) //Set the style of parent
            dialogPane = cp
            //NOTE: if its not getting focus, check Window Manager's policy
            showAndWait().ifPresent {
                //Only will enter here in OK click
                if(!cp.selectedClass) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION)
                    alert.with {
                        title = "Input Value"
                        headerText = "Info!"
                        contentText = "Class name is required!"
                        dialogPane.onKeyPressed = {
                            KeyEvent keyEvent ->
                                if (keyEvent.code == KeyCode.ESCAPE) {
                                    removeBoundingBox(groupForRectangles.children.size() - 1)
                                }
                        }
                        initModality(Modality.NONE)
                        showAndWait().ifPresent({
                            ButtonType rs ->
                                if (rs == ButtonType.OK) {
                                    close()
                                    removeBoundingBox(groupForRectangles.children.size() - 1)
                                }
                        })
                    }
                }
            }
        }
        oclass = cp.selectedClass
        //Invert start/end in case box is drawn reversed
        if(currentEndingPointX < startingPointX) {
            double tmpPoint = startingPointX
            startingPointX = currentEndingPointX
            currentEndingPointX = tmpPoint
        }
        if(currentEndingPointY < startingPointY) {
            double tmpPoint = startingPointY
            startingPointY = currentEndingPointY
            currentEndingPointY = tmpPoint
        }
        //TODO: when using fixed size, it must not fall outside the image
        //TODO: when pressing "C" and selecting a "0,0" it should ask for those values or cancel or restrict
        if (oclass) {
            double width = Math.abs(startingPointX - currentEndingPointX) ?: oclass.width
            double height = Math.abs(startingPointY - currentEndingPointY) ?: oclass.height
            if(width == 0 || height == 0) {
                width = (currentEndingPointX - startingPointX) ?: 64
                height = (currentEndingPointY - startingPointY) ?: 64
            }
            rectangleObject = new Rectangle(startingPointX, startingPointY , width, height)
            Class cls = new Class(oclass.name)
            updateLvBoundingBoxes(rectangleObject, cls.name)
        } else {
            removeBoundingBox(groupForRectangles.children.size() - 1)
        }
        spMiddleBorderPane.requestFocus()
    }

    private void removeBoundingBox(int index = -1) {
        if (index != -1) {
            groupForRectangles.children.remove(index)
        } else {
            annotation.boxes.remove(annotation.selectedBox)
            annotation.unSelectBoxes()
        }
        drawBoxes()
    }

    /**
     * Update boxes after cancel
     * @param isRemoving
     * @param rectangle
     * @param classId
     */
    private void updateLvBoundingBoxes(final Rectangle rectangle, String name = "") {
        annotation.boxes << new Annotation.Box(
                cls: new Class(name),
                rect: rectangle,
                selected: true //TODO: on mouse selection change this
        )
        drawBoxes()
    }

    /**
     * draw all the rect with colors
     */
    private void updateGroupForRectangles() {
        groupForRectangles.children.clear()
        annotation.boxes.each {
            Annotation.Box box ->
                groupForRectangles.children.add(box.rect)
        }
    }

    /**
     * Draw boxes with colors
     */
    private void drawBoxes() {
        tvBoundingBoxes.items.clear()
        ObservableList<BoxProperties> classes = FXCollections.observableArrayList()

        annotation.boxes.each {
            Annotation.Box box ->
                Label label = new Label()
                label.text = "id:" + box.cls.id + ", x:" + box.rect.x + ", y:" + box.rect.y + ", h:" + box.rect.width + ", w:" + box.rect.height
                label.setTextFill(Color.web(box.cls.color))

                BoxProperties boxProperties = new BoxProperties()
                boxProperties.name = box.cls.name
                boxProperties.height = box.rect.height
                boxProperties.width = box.rect.width

                classes.add(boxProperties)
                colors.put(box.cls.name, box.cls.color)
        }

        tvBoundingBoxes.setItems(classes)
        updateGroupForRectangles()
        displayLabelOfRectangles()
    }

    // TODO: display the small version of boxes in lvImageView
    /**
     * display small version of boxes in lvImageView
     * @param imageFile
     * @param oldSize
     * @param resizedImage
     */
    private void drawSmallBoxes(File imageFile, Image oldSize, ImageView resizedImage) {
        annotation = Formats.getAnnotation(imageFile)
        annotation.boxes.each {
            Annotation.Box box ->
        }
    }

    /**
     * navigate to the next or previous image
     * @param index
     */
    private void navigateToNextImage(int index) {
        save(listOfImages[index])
    }

    private void showAlert(String title = "Alert", String message = "", String header = "Info!", int index = -1, Alert.AlertType type = Alert.AlertType.INFORMATION) {
        if (title == "Saving file" && index != -1) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION)
            alert.title = "Saving file"
            alert.headerText = "Info!"
            alert.contentText = "Do you want to save the label?"
            ButtonType btnOk = new ButtonType("OK")
            ButtonType btnCancel = new ButtonType("CANCEL")
            alert.buttonTypes.setAll(btnOk, btnCancel)
            alert.initModality(Modality.WINDOW_MODAL)
            alert.showAndWait().ifPresent({
                ButtonType rs ->
                    if (rs == btnOk) {
                        save(listOfImages[index])
                    } else {
                        clearDrawing()
                    }
                    loadImage(listOfImages[index])
                    alert.close()
            })
        } else {
            Alert alert = new Alert(type)
            alert.title = title
            alert.headerText = header
            alert.contentText = message
            alert.alertType
            alert.showAndWait().ifPresent({
                ButtonType rs ->
                    if (rs == ButtonType.OK) {
                        alert.close()
                    }
            })
        }
    }

    /**
     * display labels with respect to rectangles
     */
    private void displayLabelOfRectangles() {
        listOfLabels.clear()
        annotation.boxes.eachWithIndex {
            Annotation.Box box, int i ->
                Label label = new Label()
                label.id = i
                label.setTranslateX(box.rect.x)
                label.setTranslateY(box.rect.y)
                label.text = box.cls.name
                label.textFill = Color.BLACK
                label.style = "-fx-background-color: ${getHashCode(Color.web(box.cls.color))};"
                listOfLabels.addAll(label)
                groupForRectangles.children.add(label)
        }
    }

    private static String getHashCode(Color color) {
        return String.format( "#%02X%02X%02X",
                (int)( color.getRed() * 255 ),
                (int)( color.getGreen() * 255 ),
                (int)( color.getBlue() * 255 ) )
    }

    private void deleteSelectedBox() {
        if (annotation != null) {
            annotation.boxes.remove(annotation.selectedBox)
            drawBoxes()
            tvBoundingBoxes.refresh()
        }
        spMiddleBorderPane.requestFocus()
    }

    /**
     * clear the bounding boxes and rectangles
     */
    private void clearDrawing() {
        if (tvBoundingBoxes.items.size() > 0 && annotation != null) {
            int size = tvBoundingBoxes.items.size()
            /*annotation.boxes.each {
                Annotation.Box box, int index ->
                    annotation.selectBox(index)
                    annotation.boxes.remove(annotation.selectedBox)
                    drawBoxes()
            }*/
            annotation.boxes.clear()
        }
        tvBoundingBoxes.items.clear()
        groupForRectangles.children.clear()
        spMiddleBorderPane.requestFocus()
        //annotation = null
    }

    /**
     * save the annotation
     * @param file
     */
    private void save(File file) {
        saveAnnotations()
        loadImage(file)
    }

    /**
     * Shortcut to get the stage from Controller
     * @return
     */
    private Stage getStage() {
        return mainBorderPane.scene.window as Stage
    }

    /**
     * Set title
     * @param title
     */
    private void setTitle(String title) {
        stage.title = title + " | IntelliBox"
    }

    private void toNextImage() {
        if (index == listOfImages.size() - 1) {
            index = 0
        }
        if (isUserLocatedAnAnnotationDirectory) {
            // save the annotations to user's specified directory
            clearDrawing()
        } else {
            if (!annotation) {
                //showAlert("Current Image", "Please select an image") as EventHandler<MouseEvent>
            } else {
                // save to directory
                if (index != -1 && lvImageView.items.size() > 0) {
                    index++
                    navigateToNextImage(index)
                    Platform.runLater({
                        lvImageView.scrollTo(index)
                        lvImageView.selectionModel.select(index)
                        lvImageView.focusModel.focus(index)
                    })
                }
            }
        }
        spMiddleBorderPane.requestFocus()
    }

    private void toPreviousImage() {
        if (index <= 0) {
            index = listOfImages.size() - 1
        }
        if (isUserLocatedAnAnnotationDirectory) {
            // save the annotations to user's specified directory
            clearDrawing()
        } else {
            if (!annotation) {
                showAlert("Current Image", "Please select an image")
            } else {
                // save to directory
                //saveAnnotations(stringToFormat(cbExtensions.value), counter)
                if (index != -1  && lvImageView.items.size() > 0) {
                    index--
                    navigateToNextImage(index)
                    Platform.runLater({
                        lvImageView.scrollTo(index)
                        lvImageView.selectionModel.select(index)
                        lvImageView.focusModel.focus(index)
                    })
                }
            }
        }
        spMiddleBorderPane.requestFocus()
    }

    private void runTask(int size) {
        final double width = 300.0d
        Label labelUpdate = new Label("Loading images...")
        labelUpdate.setPrefWidth(width)
        ProgressBar progress = new ProgressBar()
        progress.setPrefWidth(width)

        VBox updatePane = new VBox()
        updatePane.setPadding(new Insets(10))
        updatePane.setSpacing(5.0d)
        updatePane.getChildren().addAll([labelUpdate, progress])

        Stage updateStage = new Stage(StageStyle.UTILITY)
        updateStage.setScene(new Scene(updatePane))
        updateStage.show()

        Task loadTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                int max = size
                /*for (int i = 1; i <= max; i++) {
                    if (isCancelled()) {
                        break
                    }
                    updateProgress(i, max)
                    updateMessage("Loading images " + String.valueOf(i) + " complete")

                    Thread.sleep(500)
                }*/
                for (int i = lvImageView.items.size(); i < max; i++) {
                    if (isCancelled()) {
                        updateStage.hide()
                    }
                    updateProgress(i, max)
                    updateMessage("Loading images " + String.valueOf(i) + " complete")
                }
                return null
            }
        }

        loadTask.onSucceeded = {
            WorkerStateEvent wse ->
                updateStage.hide()
        }

        progress.progressProperty().bind(loadTask.progressProperty())
        labelUpdate.textProperty().bind(loadTask.messageProperty())

        updateStage.show()
        new Thread(loadTask).start()
    }

    @Override
    void initialize(URL url, ResourceBundle resourceBundle) {
        bundle = resourceBundle
        mainBorderPane.prefWidthProperty().bind(mainBorderPane.widthProperty())
        mainBorderPane.prefHeightProperty().bind(mainBorderPane.heightProperty())
        lvImageView.prefHeightProperty().bind(mainBorderPane.prefHeightProperty())
        mainVBox.prefHeightProperty().bind(mainBorderPane.prefHeightProperty())
        tvBoundingBoxes.disable = true

        mainImageView.with {
            preserveRatio = true
            smooth = true
            cache = true
        }

        File userDir = new File(SysInfo.userDir)
        File folderPng = new File(userDir.path + File.separator + ICON_PATH + File.separator + "folder.png")
        File diskPng = new File(userDir.path + File.separator + ICON_PATH + File.separator + "disk.png")
        File arrow_left2Png = new File(userDir.path + File.separator + ICON_PATH + File.separator + "arrow_left2.png")
        File arrow_right2Png = new File(userDir.path + File.separator + ICON_PATH + File.separator + "arrow_right2.png")
        File arrow_dividePng = new File(userDir.path + File.separator + ICON_PATH + File.separator + "arrow_divide.png")
        File setting_tools = new File(userDir.path + File.separator + ICON_PATH + File.separator + "setting_tools.png")

        Image openIcon = new Image(folderPng.toURI().toString())
        Image diskIcon = new Image(diskPng.toURI().toString())
        Image leftIcon = new Image(arrow_left2Png.toURI().toString())
        Image rightIcon = new Image(arrow_right2Png.toURI().toString())
        Image processIcon = new Image(arrow_dividePng.toURI().toString())
        Image setting = new Image(setting_tools.toURI().toString())
        btnOpenImageDir.graphic = new ImageView(openIcon)
        btnOpenAnnotationsDir.graphic = new ImageView(diskIcon)
        btnPrevious.graphic = new ImageView(leftIcon)
        btnNext.graphic = new ImageView(rightIcon)
        btnProcess.graphic = new ImageView(processIcon)
        btnSettings.graphic = new ImageView(setting)
        btnPrevious.disable = true
        btnNext.disable = true
        tfSearch.disable = true

        directoryChooser = new DirectoryChooser()
        configuringDirectoryChooser(directoryChooser)

        lvImageView.padding = new Insets(0, 0, 5, 0)
        lvImageView.cursor = Cursor.HAND

        grpImageView.children.add(groupForRectangles)
        grpImageView.children.add(groupForGuides)

        Formats.list << new FormatJson()
        Formats.list << new FormatPascalVoc()
        Formats.list << new FormatBBoxTxt()
        Formats.list << new FormatCocoTxt()

        VBox vBox = new VBox()
        vBox.setPadding(new Insets(10))
        vBox.setSpacing(5)

        displayImageToListView(Paths.imageDirectory)
        buttonsOperations()
    }

    private void buttonsOperations() {
        tableInit()

        btnSettings.onMouseClicked = {
            MouseEvent mouseEvent ->
                Dialog cp = new Dialog()
                cp.setDialogPane(new ClassesPane(bundle))
                cp.showAndWait()
        }

        btnClear.onMouseClicked = {
            MouseEvent mouseEvent ->
                clearDrawing()
        }

        btnDelete.onMouseClicked = {
            MouseEvent mouseEvent ->
                deleteSelectedBox()
        }

        btnPrevious.onMouseClicked = {
            MouseEvent mouseEvent ->
                toPreviousImage()
        }

        btnNext.onMouseClicked = {
            MouseEvent mouseEvent ->
                toNextImage()
        }

        btnProcess.onMouseClicked = {
            MouseEvent mouseEvent ->
                viewHandler.launchProcessScene()
        }

        btnOpenAnnotationsDir.onMouseClicked = {
            MouseEvent mouseEvent ->
                /*saveDirectory = directoryChooser.showDialog(mainBorderPane.scene.window)

                if (saveDirectory == null) {
                    print("Failed to locate a directory")
                } else {
                    if (saveAnnotations()) {
                        isUserLocatedAnAnnotationDirectory = true
                        Log.i("Success!")
                    } else {
                        Log.w("Failed to create annotation directory/ies")
                    }
                }*/
        }

        btnOpenImageDir.onMouseClicked = {
            MouseEvent mouseEvent ->
                File selectedDirectory = directoryChooser.showDialog(mainBorderPane.getScene().getWindow())
                displayImageToListView(selectedDirectory)
        }

        tvBoundingBoxes.onMouseClicked = {
            MouseEvent mouseEvent ->
                int selected = 0
                if (!tvBoundingBoxes.items.empty) {
                    int sel = tvBoundingBoxes.selectionModel.selectedIndex
                    selected = sel
                    annotation.selectBox(sel)
                    tvBoundingBoxes.onKeyPressed = {
                        KeyEvent keyEvent ->
                            switch (keyEvent.code) {
                                case KeyCode.DELETE:
                                    deleteSelectedBox()
                                    break
                                case KeyCode.W:
                                case KeyCode.S:
                                case KeyCode.A:
                                case KeyCode.D:
                                    spMiddleBorderPane.requestFocus()
                                    break
                            }
                    }
                }

                if (tvBoundingBoxes.items.size() > 0) {
                    if (selected >= 0) {
                        new MoveRect(
                                annotation.selectedBox,
                                spMiddleBorderPane,
                                listOfLabels.get(selected) as Label,
                                mainImageView.image.height as int,
                                mainImageView.image.width as int
                        )
                    }
                }

                spMiddleBorderPane.requestFocus()
        }

        tfSearch.focusedProperty().addListener({
            ObservableValue<Boolean> focus, Boolean oldB, Boolean newB ->
                if(newB) {
                    tfSearch.onKeyPressed = {
                        KeyEvent keyEvent ->
                            if (keyEvent.getCode() == KeyCode.ENTER) {
                                searchFile(Paths.imageDirectory, tfSearch.text)
                            } else if (keyEvent.getCode() == KeyCode.ESCAPE) {
                                displayImageToListView(Paths.imageDirectory)
                                tfSearch.text = ""
                                mainImageView.image = null
                            } else if (keyEvent.getCode() == KeyCode.BACK_SPACE && tfSearch.text.empty) {
                                displayImageToListView(Paths.imageDirectory)
                            } else {
                                searchFile(Paths.imageDirectory, tfSearch.text)
                            }
                    }
                }
        } as ChangeListener<Boolean>)

        lvImageView.onMouseClicked = {
            MouseEvent mouseEvent ->
                index = lvImageView.selectionModel.selectedIndex
                if (mouseEvent.button == MouseButton.PRIMARY) {
                    if (groupForRectangles.children.size() > 0) {
                        if (annotation != Formats.getAnnotation(annotation.imageFile)) {
                            showAlert("Saving file", "Do you want to save the label?", "Info!", index)
                            return
                        }
                    }
                    // just navigate
                    if (index != -1) {
                        loadImage(listOfImages[index])
                        btnPrevious.disable = false
                        btnNext.disable = false
                    }
                }
                spMiddleBorderPane.requestFocus()
        }

        spMiddleBorderPane.onMouseClicked = {
            MouseEvent mouseEvent ->
                spMiddleBorderPane.onKeyPressed = {
                    KeyEvent keyEvent ->
                        switch (keyEvent.code) {
                            case KeyCode.DELETE :
                                if(index != -1) {
                                    final ContextMenu contextMenu = new ContextMenu()
                                    final MenuItem item = new MenuItem("Delete this Image")
                                    contextMenu.with {
                                        items.add(item)
                                        show(lvImageView.scene.window)
                                    }
                                    item.onAction = {
                                        ActionEvent event ->
                                            listOfImages[index].delete()
                                            listOfImages.remove(index)
                                            lvImageView.items.removeAt(index)
                                            mainImageView.image = null
                                    }
                                }
                                break
                            case KeyCode.SPACE:
                                if (!isRectangleBeingDrawn) {
                                    toNextImage()
                                } else {
                                    showAlert("Active Operation", "You have an active box, cancel first before saving your changes", "Warning!", -1, Alert.AlertType.WARNING)
                                }
                                break
                            case KeyCode.BACK_SPACE:
                                if (!isRectangleBeingDrawn) {
                                    toPreviousImage()
                                } else {
                                    showAlert("Active Operation", "You have an active box, cancel first before saving your changes", "Warning!", -1, Alert.AlertType.WARNING)
                                }
                                break
                            case KeyCode.ESCAPE:
                                cancelDrawingBox()
                                break
                            case KeyCode.X:
                                drawBox(currentEndingPointX, currentEndingPointY)
                                break
                            case KeyCode.C:
                                if (isRectangleBeingDrawn) {
                                    cancelDrawingBox()
                                }
                                startingPointX = currentEndingPointX
                                startingPointY = currentEndingPointY
                                showClassesDialog()
                                break
                        }

                        if (keyEvent.getCode() == KeyCode.UP || keyEvent.getCode() == KeyCode.DOWN || keyEvent.getCode() == KeyCode.LEFT || keyEvent.getCode() == KeyCode.RIGHT) {
                            keyEvent.consume()
                        }
                }
        }

        spMiddleBorderPane.onKeyPressed = {
            KeyEvent keyEvent ->
                switch (keyEvent.code) {
                    case KeyCode.DELETE :
                        if(index != -1) {
                            final ContextMenu contextMenu = new ContextMenu()
                            final MenuItem item = new MenuItem("Delete this Image")
                            contextMenu.with {
                                items.add(item)
                                show(lvImageView.scene.window)
                            }
                            item.onAction = {
                                ActionEvent event ->
                                    listOfImages[index].delete()
                                    listOfImages.remove(index)
                                    lvImageView.items.removeAt(index)
                                    mainImageView.image = null
                            }
                        }
                        break
                    case KeyCode.SPACE:
                        toNextImage()
                        break
                    case KeyCode.BACK_SPACE:
                        toPreviousImage()
                        break
                    case KeyCode.ESCAPE:
                        cancelDrawingBox()
                        break
                    case KeyCode.X:
                        drawBox(currentEndingPointX, currentEndingPointY)
                        break
                    case KeyCode.C:
                        startingPointX = currentEndingPointX
                        startingPointY = currentEndingPointY
                        showClassesDialog()
                        break
                }

                if (keyEvent.getCode() == KeyCode.UP || keyEvent.getCode() == KeyCode.DOWN || keyEvent.getCode() == KeyCode.LEFT || keyEvent.getCode() == KeyCode.RIGHT) {
                    keyEvent.consume()
                }
        }

        btnSearch.onMouseClicked = {
            MouseEvent mouseEvent ->
                if (tfSearch.text.empty) {
                    showAlert("Empty", "Search field is empty!", "Warning!", -1, Alert.AlertType.WARNING)
                } else {
                    searchFile(Paths.imageDirectory, tfSearch.text)
                }
        }
    }

    private void tableInit() {
        // name column
        tcClassName.cellValueFactory = new PropertyValueFactory<BoxProperties, String>("name")
        tcClassName.cellFactory = cellFactory
        // height column
        tcHeight.cellValueFactory = new PropertyValueFactory<BoxProperties, Integer>("height")
        // width column
        tcWidth.cellValueFactory = new PropertyValueFactory<BoxProperties, Integer>("width")
    }

    Callback<TableColumn<BoxProperties, String>, TableCell<BoxProperties, String>> cellFactory =
            new Callback<TableColumn<BoxProperties, String>, TableCell<BoxProperties, String>>() {
                TableCell call(TableColumn p) {
                    TableCell cell = new TableCell<BoxProperties, String>() {
                        @Override
                        void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty)
                            setText(empty ? null : getString())
                            if (colors.containsKey(getString())) {
                                //Log.i(colors.get(getString()))
                                setStyle("-fx-background-color: " + colors.get(getString()))
                            } else {
                                setStyle("")
                            }
                        }

                        private String getString() {
                            return getItem() == null ? "" : getItem().toString()
                        }
                    }

                    return cell
                }
            }

    private static final class MouseLocation {
        public double x, y
    }
}
