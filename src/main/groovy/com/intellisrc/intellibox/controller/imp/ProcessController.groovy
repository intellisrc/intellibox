package com.intellisrc.intellibox.controller.imp

import com.intellisrc.intellibox.handler.ViewHandler
import com.intellisrc.intellibox.controller.AbstractController
import groovy.transform.CompileStatic
import javafx.fxml.FXML
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.TextField
import javafx.scene.input.MouseEvent
import javafx.scene.layout.AnchorPane
import javafx.stage.DirectoryChooser
import javafx.stage.Stage

import com.intellisrc.intellibox.util.Process

@CompileStatic
class ProcessController extends AbstractController {
    // panes
    @FXML
    private AnchorPane mainAnchorPane
    // checkfields
    @FXML
    private TextField tfTrain
    @FXML
    private TextField tfTest
    @FXML
    private TextField tfImagesDir
    @FXML
    private TextField tfAnnotationsDir
    @FXML
    private TextField tfPercentage
    // buttons
    @FXML
    private Button btnProcess
    @FXML
    private Button btnAnnotationsDir
    @FXML
    private Button btnImagesDir

    // javafx
    private DirectoryChooser directoryChooser

    // classes
    private Process process

    ProcessController(ViewHandler viewHandler) {
        super(viewHandler)
    }

    @Override
    void initialize(URL url, ResourceBundle resourceBundle) {
        process = new Process()
        directoryChooser = new DirectoryChooser()
        buttonsOperations(directoryChooser)
    }

    private void buttonsOperations(DirectoryChooser directoryChooser) {
        File imageDirectory
        File annotationsDirectory

        btnImagesDir.setOnMouseClicked({
            MouseEvent mouseEvent ->
                assert mainAnchorPane.scene.window != null
                imageDirectory = directoryChooser.showDialog(mainAnchorPane.scene.window)
                if (imageDirectory != null)
                    tfImagesDir.text = imageDirectory.getAbsolutePath()
        })

        btnAnnotationsDir.setOnMouseClicked({
            MouseEvent mouseEvent ->
                annotationsDirectory = directoryChooser.showDialog(mainAnchorPane.scene.window)
                if (annotationsDirectory != null)
                    tfAnnotationsDir.text = annotationsDirectory.path
        })

        btnProcess.setOnMouseClicked({
            MouseEvent mouseEvent ->
                viewHandler.transportValue("")
                if (tfImagesDir.text == "") {
                    showAlert("Directory", "Images directory is missing")
                    return
                } else if (tfAnnotationsDir.text == "") {
                    showAlert("Directory", "Annotations directory is missing")
                    return
                } else if (tfImagesDir.text == "" && tfAnnotationsDir.text == "") {
                    showAlert("Directory", "Images/Annotations directory is missing")
                    return
                } else if (imageDirectory == null) {
                    showAlert("Directory", "Wrong image directory! ${tfImagesDir.text}", "Error!", Alert.AlertType.ERROR)
                    return
                } else if (tfPercentage.text == "" || tfPercentage.text ==  /\[a-zA-Z]/) {
                    showAlert("Digit", "Non digit ${tfPercentage.text}", "Error!", Alert.AlertType.ERROR)
                    return
                }
                process.startProcessing(imageDirectory.path.toString(), annotationsDirectory.path.toString(),
                        tfTrain.text + ".txt", tfTest.text + ".txt", tfPercentage.text.toString().replaceFirst("[^x0-9]", "").toInteger())
                Stage stage = (Stage) btnProcess.scene.window
                stage.close()
        })
    }

    private static void showAlert(String title = "Alert", String message = "", String header = "Info!", Alert.AlertType type = Alert.AlertType.INFORMATION) {
        Alert alert = new Alert(type)
        alert.title = title
        alert.headerText = header
        alert.contentText = message
        alert.alertType
        alert.showAndWait().ifPresent({
            ButtonType rs ->
                if (rs == ButtonType.OK) {
                    alert.close()
                }
        })
    }
}
