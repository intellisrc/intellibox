package com.intellisrc.intellibox.controller.imp

import com.intellisrc.intellibox.controller.AbstractController
import com.intellisrc.intellibox.handler.ViewHandler
import groovy.transform.CompileStatic
import javafx.fxml.FXML
import javafx.scene.Group
import javafx.scene.Node
import javafx.scene.control.CheckBox
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage

@CompileStatic
class BoxesController extends AbstractController {
    @FXML
    private Group grpBoxesMain
    @FXML
    private AnchorPane anchorPaneMain
    // checkboxes
    @FXML
    private CheckBox cbMM
    @FXML
    private CheckBox cbMI
    @FXML
    private CheckBox cbMC
    @FXML
    private CheckBox cbMH
    @FXML
    private CheckBox cbME
    @FXML
    private CheckBox cbML
    @FXML
    private CheckBox cbMN
    @FXML
    private CheckBox cbDD
    @FXML
    private CheckBox cbDU
    @FXML
    private CheckBox cbDN
    @FXML
    private CheckBox cbDL
    @FXML
    private CheckBox cbDO
    @FXML
    private CheckBox cbDP
    @FXML
    private CheckBox cbBB
    @FXML
    private CheckBox cbBR
    @FXML
    private CheckBox cbBI
    @FXML
    private CheckBox cbBD
    @FXML
    private CheckBox cbBG
    @FXML
    private CheckBox cbBE
    @FXML
    private CheckBox cbBS
    @FXML
    private CheckBox cbBT
    @FXML
    private CheckBox cbBO
    @FXML
    private CheckBox cbBN
    @FXML
    private CheckBox cbYY
    @FXML
    private CheckBox cbYO
    @FXML
    private CheckBox cbYK
    @FXML
    private CheckBox cbYH
    @FXML
    private CheckBox cbYA
    @FXML
    private CheckBox cbYM

    BoxesController(ViewHandler viewHandler) {
        super(viewHandler)
    }

    private Stage getStage() {
        return anchorPaneMain.scene.window as Stage
    }

    @Override
    void initialize(URL url, ResourceBundle resourceBundle) {
        /*anchorPaneMain.children.each {
            Node node ->
                if (node instanceof CheckBox) {
                    node as CheckBox
                    node.selected = node.focused
                }
        }*/

        for (Node node in anchorPaneMain.getChildren()) {
            if (node instanceof CheckBox) {
                node as CheckBox
                if (node.isFocused()) {
                    node.setSelected(true)
                } else {
                    node.setSelected(false)
                }
            }
        }

        anchorPaneMain.onKeyPressed = {
            KeyEvent keyEvent ->
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    stage.close()
                }
        }
    }
}
