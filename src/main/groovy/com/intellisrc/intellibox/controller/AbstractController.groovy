package com.intellisrc.intellibox.controller

import com.intellisrc.intellibox.handler.ViewHandler
import groovy.transform.CompileStatic
import javafx.fxml.Initializable

@CompileStatic
abstract class AbstractController implements Initializable {
    ViewHandler viewHandler

    AbstractController(ViewHandler viewHandler) {
        this.viewHandler = viewHandler
    }

    @Override
    abstract void initialize(URL url, ResourceBundle resourceBundle)
}
