package com.intellisrc.intellibox.view.dialog

import com.intellisrc.core.Log
import com.intellisrc.core.SysInfo
import com.intellisrc.intellibox.util.Classes
import com.intellisrc.intellibox.util.Paths

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.fxml.FXMLLoader
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.control.DialogPane
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.control.TextFormatter
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import java.util.function.UnaryOperator

import groovy.transform.CompileStatic
import javafx.fxml.FXML

@CompileStatic
class ClassesPane extends DialogPane {
    // pane
    @FXML
    DialogPane mainDialogPane
    @FXML
    private BorderPane borderPaneMain
    // table view
    @FXML
    private TableView<Classes.ObjClass> tvClassesDimensions
    // table column
    @FXML
    private TableColumn<Classes.ObjClass, String> tcClassName
    @FXML
    private TableColumn<Classes.ObjClass, Integer> tcHeight
    @FXML
    private TableColumn<Classes.ObjClass, Integer> tcWidth
    @FXML
    private TableColumn<Classes.ObjClass, String> tcShortcut
    // buttons
    @FXML
    private Button btnAddClasses
    // text fields
    @FXML
    private TextField tfName
    @FXML
    private TextField tfWidth
    @FXML
    private TextField tfHeight
    @FXML
    private TextField tfShortcut

    List<String> shortcutList = []
    Classes.ObjClass selectedClass = null
    public static final ButtonType OK = new ButtonType("OK")

    ClassesPane(ResourceBundle bundle) {
        String fxmlFileName = "classes.fxml"
        URL url = new File(SysInfo.userDir + Paths.FXML_PATH + fxmlFileName).toURI().toURL()
        FXMLLoader fxmlLoader = new FXMLLoader(url ,bundle)
        fxmlLoader.root = this
        fxmlLoader.controller = this
        fxmlLoader.load()
        populateListView()
        eventsOperation()
    }

    private Stage getStage() {
        return borderPaneMain.scene.window as Stage
    }

    private void editClass() {
        if(selectedClass) {
            tfName.text = selectedClass.name
            tfHeight.text = selectedClass.height
            tfWidth.text = selectedClass.width
            tfShortcut.text = selectedClass.shortcuts
            btnAddClasses.text = "Save"
        }
    }

    private void loadShortCutList() {
        Classes.classes.each {
            shortcutList << it.shortcuts
        }
    }

    private void setAutoKeyShort() {
        String shortcut = ""
        loadShortCutList()
        if(tfName.text &&! tfShortcut.text) {
            if((0..tfName.text.length() - 1).any {
                int ch ->
                    shortcut = tfName.text.substring(ch, ch + 1)
                    return !shortcutList.find { it.toUpperCase() == shortcut.toUpperCase() }
            }) {
                tfShortcut.text = shortcut.toUpperCase()
            }
        }
    }

    private void saveSettings() {
        int width = Integer.parseInt(tfWidth.text ?: "0")
        int height = Integer.parseInt(tfHeight.text ?: "0")
        if (!tfName.text.empty && (tfWidth.text.empty == tfHeight.text.empty)) {
            setAutoKeyShort()
            if (selectedClass) {
                selectedClass.name = tfName.text
                selectedClass.height = height
                selectedClass.width = width
                selectedClass.importShortCuts(tfShortcut.text)
            } else {
                Classes.classes << new Classes.ObjClass(
                        name: tfName.text,
                        height: height,
                        width: width
                ).importShortCuts(tfShortcut.text)
            }
            Classes.save()
            populateListView()
            resetInputs()
        } else {
            Log.w("Incorrect input")
        }
    }

    private void resetInputs() {
        tfName.text = ""
        tfHeight.text = ""
        tfWidth.text = ""
        tfShortcut.text = ""
        tfName.requestFocus()
        btnAddClasses.text = "Add"
        selectedClass = null
    }

    private void eventsOperation() {
        def digitsOnlyOperator = new UnaryOperator<TextFormatter.Change>() {
            @Override
            TextFormatter.Change apply(TextFormatter.Change change) {
                return !change.contentChange || (change.controlNewText.empty || change.controlNewText.isNumber()) ? change : null
            }
        }
        mainDialogPane.onKeyPressed = {
            KeyEvent keyEvent ->
                switch (keyEvent.code) {
                    case KeyCode.ESCAPE:
                        if(selectedClass) {
                            resetInputs()
                        } else {
                            stage.close()
                        }
                        break
                    case KeyCode.DELETE:
                        Classes.classes.remove(selectedClass)
                        saveSettings()
                        break
                    default:
                        Classes.ObjClass co = Classes.classes.find {
                            it.shortcutKeys.contains(keyEvent.code)
                        }
                        if(co) {
                            selectedClass = co
                            tvClassesDimensions.selectionModel.select(co)
                            editClass()
                            stage.close()
                        }
                        break
                }

        }
        tfHeight.textFormatter = new TextFormatter<>(digitsOnlyOperator)
        tfWidth.textFormatter = new TextFormatter<>(digitsOnlyOperator)
        def saveOnEnter = {
            KeyEvent keyEvent ->
                if(keyEvent.code == KeyCode.ENTER) {
                    saveSettings()
                }
        }
        tfName.onKeyPressed     = saveOnEnter
        tfWidth.onKeyPressed    = saveOnEnter
        tfHeight.onKeyPressed   = saveOnEnter
        tfShortcut.onKeyPressed = saveOnEnter

        btnAddClasses.onMouseClicked = {
            MouseEvent mouseEvent ->
                saveSettings()
        }
        tvClassesDimensions.with {
            onMouseClicked = {
                MouseEvent mouseEvent ->
                    if (mouseEvent.button == MouseButton.PRIMARY) {
                        selectedClass = tvClassesDimensions.selectionModel.selectedItem
                        editClass()
                    }
            }
        }
    }

    private void populateListView() {
        tableInit()
        tvClassesDimensions.items.clear()
        ObservableList<Classes.ObjClass> objClass = FXCollections.observableArrayList()
        Classes.classes.each {
            Classes.ObjClass obj ->
                objClass.add(obj)
                Log.v(obj.name + " : " + obj.height + " : " + obj.width)
        }

        tvClassesDimensions.setItems(objClass)
    }

    private void tableInit() {
        // name column
        tcClassName.cellValueFactory = new PropertyValueFactory<Classes.ObjClass, String>("name")
        // height column
        tcHeight.cellValueFactory = new PropertyValueFactory<Classes.ObjClass, Integer>("height")
        // width column
        tcWidth.cellValueFactory = new PropertyValueFactory<Classes.ObjClass, Integer>("width")
        // shortcut column
        tcShortcut.cellValueFactory = new PropertyValueFactory<Classes.ObjClass, String>("shortcuts")
    }

    @Override
    Node createButton(ButtonType buttonType) {
        return super.createButton(buttonType) as Button
    }

}