package com.intellisrc.intellibox.view.window.imp

import com.intellisrc.intellibox.controller.AbstractController
import com.intellisrc.intellibox.view.AbstractWindow

class ClassesWindow extends AbstractWindow {

    ClassesWindow(AbstractController controller, ResourceBundle bundle) {
        super(controller, bundle)
    }

    @Override
    protected String iconFileName() {
        return null
    }

    @Override
    protected String fxmlFileName() {
        return "settings.fxml"
    }

    @Override
    String titleBundleKey() {
        return "settings.title"
    }
}
