package com.intellisrc.intellibox.view.window.imp

import com.intellisrc.intellibox.controller.AbstractController
import com.intellisrc.intellibox.view.AbstractWindow
import groovy.transform.CompileStatic

@CompileStatic
class ProcessWindow extends AbstractWindow {

    ProcessWindow(AbstractController controller, ResourceBundle bundle) {
        super(controller, bundle)
    }

    @Override
    protected String iconFileName() {
        return null
    }

    @Override
    protected String fxmlFileName() {
        return "process.fxml"
    }

    @Override
    String titleBundleKey() {
        return "process.title"
    }
}
