package com.intellisrc.intellibox.view.window.imp

import com.intellisrc.intellibox.controller.AbstractController
import com.intellisrc.intellibox.controller.imp.BoxesController
import com.intellisrc.intellibox.controller.imp.LabelController
import com.intellisrc.intellibox.controller.imp.ProcessController
import com.intellisrc.intellibox.handler.ViewHandler
import com.intellisrc.intellibox.view.AbstractWindow
import groovy.transform.CompileStatic

@CompileStatic
enum WindowFactory {
    MAIN {
        @Override
        AbstractWindow createWindow(ViewHandler viewHandler, ResourceBundle bundle) {
            final AbstractController controller = new LabelController(viewHandler)
            return new MainWindow(controller, bundle)
        }
    },

    PROCESS {
        @Override
        AbstractWindow createWindow(ViewHandler viewHandler, ResourceBundle bundle) {
            final AbstractController controller = new ProcessController(viewHandler)
            return new ProcessWindow(controller, bundle)
        }
    },

    BOXES {
        @Override
        AbstractWindow createWindow(ViewHandler viewHandler, ResourceBundle bundle) {
            final AbstractController controller = new BoxesController(viewHandler)
            return new BoxesWindow(controller, bundle)
        }
    },

    abstract AbstractWindow createWindow(ViewHandler viewHandler, ResourceBundle bundle)
}
