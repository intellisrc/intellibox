package com.intellisrc.intellibox.view.window.imp

import com.intellisrc.intellibox.controller.AbstractController
import com.intellisrc.intellibox.view.AbstractWindow
import groovy.transform.CompileStatic

@CompileStatic
class MainWindow extends AbstractWindow {

    MainWindow(AbstractController controller, ResourceBundle bundle) {
        super(controller, bundle)
    }

    @Override
    protected String iconFileName() {
        return "intellibox.png"
    }

    @Override
    protected String fxmlFileName() {
        return "label.fxml"
    }

    @Override
    String titleBundleKey() {
        return "main.title"
    }
}
