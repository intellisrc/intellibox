package com.intellisrc.intellibox.view

import com.intellisrc.core.SysInfo
import com.intellisrc.intellibox.controller.AbstractController
import com.intellisrc.intellibox.util.Paths
import groovy.transform.CompileStatic
import javafx.fxml.FXMLLoader
import javafx.scene.Parent

@CompileStatic
abstract class AbstractWindow {
    private final AbstractController controller
    private final ResourceBundle bundle

    AbstractWindow(AbstractController controller, ResourceBundle bundle) {
        this.controller = controller
        this.bundle = bundle
    }

    Parent root() throws IOException {
        FXMLLoader loader = new FXMLLoader(url(), bundle)
        loader.setController(controller)
        return loader.load()
    }

    private URL url() {
        return new File(SysInfo.userDir + Paths.FXML_PATH + fxmlFileName()).toURI().toURL()
    }

    String iconFilePath() {
        println SysInfo.userDir + Paths.ICON_PATH + iconFileName()
        return SysInfo.userDir + Paths.ICON_PATH + iconFileName()
    }

    boolean resizable() {
        return true
    }

    protected abstract String iconFileName();
    protected abstract String fxmlFileName();
    abstract String titleBundleKey();
}
