package com.intellisrc.intellibox.view

import com.intellisrc.core.SysInfo
import com.intellisrc.intellibox.handler.AppViewHandler
import javafx.application.Platform
import javafx.scene.image.Image

import static com.intellisrc.intellibox.util.Paths.ICON_PATH
import static com.intellisrc.intellibox.util.Paths.RESOURCE_BUNDLE
import groovy.transform.CompileStatic
import javafx.application.Application
import javafx.stage.Stage

/**
 * @since 18/08/20.
 */
@CompileStatic
class LabelUI extends Application {
    private Image icon
    private ResourceBundle bundle
    private Stage main

    @Override
    void init() throws Exception {
        super.init()
        File file = new File(SysInfo.userDir)
        URL urls = file.toURI().toURL()
        ClassLoader loader = new URLClassLoader(urls)
        bundle = ResourceBundle.getBundle(RESOURCE_BUNDLE, Locale.default, loader)
        icon = new Image(new File(file.path + File.separator + ICON_PATH + "intellibox.png").toURI().toString())
    }

    @Override
    void start(Stage stage) throws Exception {
        //Platform.setImplicitExit(false)
        main = stage
        new AppViewHandler(stage, bundle, icon).launchMainScene()
    }

    @Override
    void stop() throws Exception {
        super.stop()
    }
}
