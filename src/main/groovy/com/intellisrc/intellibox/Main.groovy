package com.intellisrc.intellibox

import com.intellisrc.core.SysMain
import com.intellisrc.intellibox.util.Paths
import com.intellisrc.intellibox.view.LabelUI
import groovy.transform.CompileStatic
import javafx.application.Application

/**
 * Main class (for Jar)
 * @since 18/09/05.
 */
@CompileStatic
class Main extends SysMain {
    static {
        main = new Main()
    }
    @Override
    void onStart() {
        if(!args.empty) {
            Paths.imageDirectory = new File(args.poll())
        }
        Application.launch(LabelUI.class)
    }
}