package com.intellisrc.intellibox.handler

import groovy.transform.CompileStatic

@CompileStatic
interface ViewHandler {
    void launchMainScene() throws IOException
    void launchProcessScene() throws IOException
    void launchBoxesScene() throws IOException
    void transportValue(String value)
    String getTransportedValue()
}