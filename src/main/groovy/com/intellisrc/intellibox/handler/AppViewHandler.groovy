package com.intellisrc.intellibox.handler

import com.intellisrc.intellibox.view.AbstractWindow
import com.intellisrc.intellibox.view.window.imp.WindowFactory
import groovy.transform.CompileStatic
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.WindowEvent

@CompileStatic
class AppViewHandler implements ViewHandler {
    private final Stage main
    private final ResourceBundle bundle
    private final Image icon
    private String value

    AppViewHandler(Stage main, ResourceBundle bundle, Image icon) {
        this.main = main
        this.bundle = bundle
        this.icon = icon
    }

    @Override
    void launchMainScene() throws IOException {
        buildAndShowScene(main, WindowFactory.MAIN.createWindow(this, bundle))
    }

    @Override
    void launchProcessScene() throws IOException {
        Stage processStage = new Stage()
        processStage.initModality(Modality.WINDOW_MODAL)
        buildAndShowScene(processStage, WindowFactory.PROCESS.createWindow(this, bundle))
    }

    @Override
    void launchBoxesScene() throws IOException {
        Stage boxesStage = new Stage()
        boxesStage.initModality(Modality.WINDOW_MODAL)
        buildAndShowScene(boxesStage, WindowFactory.BOXES.createWindow(this, bundle))
    }

    @Override
    void transportValue(String value) {
        this.value = value
    }

    @Override
    String getTransportedValue() {
        return value
    }

    private void buildAndShowScene(Stage stage, AbstractWindow window) throws IOException {
        try {
            stage.icons.add(icon)
        } catch(Exception e) {
            e.printStackTrace()
        }

        if (window.titleBundleKey() == "process.title") {
            stage.setTitle(bundle.getString(window.titleBundleKey()))
            stage.setResizable(window.resizable())
            stage.scene = new Scene(window.root())
        } else if (window.titleBundleKey() == "boxes.title") {
            stage.setTitle(bundle.getString(window.titleBundleKey()))
            stage.setResizable(window.resizable())
            stage.scene = new Scene(window.root())
        } else if (window.titleBundleKey() == "settings.title") {
            stage.setTitle(bundle.getString(window.titleBundleKey()))
            stage.setResizable(window.resizable())
            stage.scene = new Scene(window.root())
        } else {
            stage.setTitle(bundle.getString(window.titleBundleKey()))
            stage.setResizable(window.resizable())
            stage.scene = new Scene(window.root(), 1096, 720)
        }
        stage.onCloseRequest = {
            WindowEvent windowEvent ->
                stage.close()
        }
        stage.scene.stylesheets.clear()
        stage.scene.stylesheets.add(this.class.getResource("/css/darktheme.css").toURI().toString())
        stage.show()
    }
}
