package com.intellisrc.intellibox.util

import com.intellisrc.core.Log
import com.intellisrc.web.JSON
import groovy.transform.CompileStatic
import javafx.scene.input.KeyCode

/**
 * @since 19/01/09.
 */
@CompileStatic
class Classes {
    static File classesCfgFile = new File(Paths.imageDirectory, "classes.json")
    static class ObjClass {
        String name
        int height
        int width
        LinkedList<KeyCode> shortcutKeys = [] as LinkedList<KeyCode> // example: [ "CONTROL", "Z" ], [ "A", "B" ]
        Map toMap() {
            return [
                name        : name,
                height      : height,
                width       : width,
                shortcuts   : shortcuts
            ]
        }
        void addShortCut(String shortcut) {
            KeyCode kc = KeyCode.getKeyCode(shortcut.toUpperCase())
            if(kc) {
                shortcutKeys << kc
            } else {
                Log.w("KeyCode was not recognized: %s", shortcut.toUpperCase())
            }
        }
        //See: https://docs.oracle.com/javafx/2/api/javafx/scene/input/KeyCode.html
        ObjClass importShortCuts(String shortcutList) {
            shortcutKeys = [] as LinkedList<KeyCode>
            if(shortcutList) {
                shortcutList.tokenize(',').each { addShortCut(it) }
            }
            return this
        }
        String getShortcuts() {
            return shortcutKeys.empty ? '' : shortcutKeys.collect { it.name }.join(',')
        }
    }
    static List<ObjClass> classes = []

    static {
        Log.i("Classes config location: %s", classesCfgFile.absolutePath)
        if (classesCfgFile.exists()) {
            load()
        }
    }

    static void load() {
        Log.i("Classes config file loaded")
        if(!classesCfgFile.text.empty) {
            def json = JSON.decode(classesCfgFile.text).toMap()
            json.classes.each {
                Map oclass = it as Map
                classes << new ObjClass(
                        name: oclass.name.toString(),
                        height: oclass.height as int,
                        width: oclass.width as int,
                ).importShortCuts(oclass.shortcuts as String)
            }
        } else {
            classesCfgFile.delete()
        }
    }

    static void save() {
        Log.i("Classes config file saved")
        classesCfgFile.text = JSON.encode([
                classes : classes.collect { it.toMap() }
        ], true)
    }
}
