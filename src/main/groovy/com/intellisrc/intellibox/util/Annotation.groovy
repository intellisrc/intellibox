package com.intellisrc.intellibox.util

import com.intellisrc.core.Log
import groovy.transform.CompileStatic
import javafx.scene.image.Image
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle

/**
 * @since 18/08/31.
 */
@CompileStatic
class Annotation {
    File imageFile
    Image image
    static class Box {
        Class cls
        Rectangle rect
        boolean selected

        Rectangle getRect() {
            rect.setFill(Color.TRANSPARENT)
            rect.setStroke(Color.web(cls.color))
            return rect
        }

        @Override
        String toString() {
            return "${cls.id}:${cls.name} -> ${rect.x},${rect.y},${rect.width},${rect.height}"
        }
    }
    final List<Box> boxes = []

    Annotation(final File file, Image image = null) {
        imageFile = file
        this.image = image
    }

    Image getImage() {
        return image ?: new Image(new FileInputStream(imageFile))
    }

    Box getSelectedBox() {
        Box box = boxes.find { it.selected }
        if(box) {
            Log.i("Selected box : " + box.toString())
        }
        return box
    }

    void unSelectBoxes() {
        boxes.each { it.selected = false }
    }

    void selectBox(int index) {
        unSelectBoxes()
        boxes[index].selected = true
    }

    // function that will compare the annotation
    boolean equals(Annotation other) {
        boolean equal = true
        try {
            assert imageFile.name == other.imageFile.name
            assert boxes.size() == other.boxes.size()
            boxes.eachWithIndex {
                Box box, int index ->
                    Box otherBox = other.boxes[index]
                    assert box.cls.id == otherBox.cls.id
                    ["x", "y", "width", "height"].each {
                        assert (box.rect[it] as int) == (otherBox.rect[it] as int)
                    }
            }
        } catch(AssertionError error) {
            Log.v("Annotations were not the same: %s", error.message)
            equal = false
        }
        return equal
    }

    Map<String,Object> toMap() {
        List<Map<String,Integer>> boxesList = boxes.collect {
            return  [
                    id : it.cls.id,
                    x : it.rect.x as int,
                    y : it.rect.y as int,
                    w : it.rect.width as int,
                    h : it.rect.height as int
            ] as Map<String,Integer>
        }
        Map<String,Object> map = [
                file : imageFile.name,
                image : [
                        width : getImage().width as int,
                        height: getImage().height as int
                ],
                classes : Class.listAsMap(),
                boxes : boxesList
        ]
        return map
    }
}