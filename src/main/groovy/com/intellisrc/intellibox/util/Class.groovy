package com.intellisrc.intellibox.util

import com.intellisrc.core.Config
import com.intellisrc.core.Log
import groovy.transform.CompileStatic
import javafx.scene.paint.Color

/**
 * @since 18/09/03.
 */
@CompileStatic
class Class {
    static {
        if(Config.hasKey("box.colors")) {
            Config.get("box.colors").split(",").each {
                String c ->
                    //colors << Color.web(c.trim().toUpperCase())
            }
        } else {
            colors = ["#FF0000", "#0000FF", "#00FF00",
             "#FFFF00", "#00FFFF", "#FFA500", "#32CD32",
             "#F0F8FF", "#FAEBD7", "#00FFFF", "#7FFFD4",
             "#F0FFFF", "#FF00FF", "#A0522D", "#4682B4",
             "#FFE4C4", "#FFEBCD", "#8A2BE2", "#A52A2A",
             "#DEB887", "#5F9EA0", "#7FFF00", "#D2691E",
             "#FF7F50", "#6495ED", "#FFF8DC", "#DC143C",
             "#00008B", "#008B8B", "#B8860B", "#F5F5DC",
             "#A9A9A9", "#006400", "#A9A9A9", "#BDB76B",
             "#8B008B", "#556B2F", "#FF8C00", "#9932CC",
             "#8B0000", "#E9967A", "#8FBC8F", "#483D8B",
             "#2F4F4F", "#00CED1", "#9400D3", "#FF1493",
             "#00BFFF", "#696969", "#1E90FF", "#FF00FF",
             "#B22222", "#FFFAF0", "#228B22", "#DCDCDC",
             "#FFD700", "#DAA520", "#ADFF2F", "#7CFC00",
             "#F0FFF0", "#FF69B4", "#CD5C5C", "#4B0082",
             "#FFFFF0", "#F0E68C", "#E6E6FA", "#FFF0F5",
             "#FFFACD", "#ADD8E6", "#F08080", "#E0FFFF",
             "#FAFAD2", "#D3D3D3", "#90EE90", "#F8F8FF",
             "#D3D3D3", "#FFB6C1", "#FFA07A", "#20B2AA",
             "#87CEFA", "#778899", "#778899", "#BA55D3",
             "#B0C4DE", "#FFFFE0", "#800000", "#0000CD",
             "#66CDAA", "#66CDAA",]
        }
    }
    static final synchronized List<Class> list = []
    private static List<String> colors = []

    final int id
    final String color
    String name

    Class(final String name, int classId = -1) {
        Class cls = list.find { it.name == name }
        if (cls) {
            this.id = cls.id
            this.color = cls.color
            this.name = cls.name
        } else {
            Log.i("Creating new class: $name")
            if(classId >= 0) {
                this.id = list.find { it.id == classId } ? nextId : classId
            } else {
                this.id = nextId
            }
            this.color = colors[id]
            this.name = name
            list << this
        }
    }

    /**
     * Static shortcut to add class
     * @param name
     * @param classId
     */
    static void addClass(String name, int classId = -1) {
        new Class(name, classId)
    }

    static Class fromId(int id) {
        return list.find { it.id == id }
    }

    static int getNextId() {
        return list.size()
    }
    /**
     * Return a copy of the class list as a map
     * @return
     */
    static Map<String, String> listAsMap() {
        return list.collectEntries {[
                (it.id) : it.name
        ]}
    }
}
