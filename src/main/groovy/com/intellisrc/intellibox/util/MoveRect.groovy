package com.intellisrc.intellibox.util

import groovy.transform.CompileStatic
import javafx.animation.AnimationTimer
import javafx.event.EventHandler
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.shape.Rectangle

@CompileStatic
class MoveRect {
    private Rectangle rect
    private double paddleY = 250
    private double paddleX = 250
    private double paddleDY
    private double paddleDX
    private ScrollPane pane
    private Label label
    private int imgHeight, imgWidth

    MoveRect(Annotation.Box rect, ScrollPane pane, Label label, int imgHeight, int imgWidth) {
        this.rect = rect.rect
        this.pane = pane
        this.label = label
        this.imgHeight = imgHeight
        this.imgWidth = imgWidth
        this.paddleY = this.rect.y
        this.paddleX = this.rect.x

        this.pane.onKeyPressed = keyPressed
        this.pane.onKeyReleased = keyReleased
        timer.start()
    }

    private AnimationTimer timer = new AnimationTimer() {
        @Override
        void handle(long l) {
            paddleY += paddleDY
            paddleX += paddleDX
            if (paddleY < 0)
                paddleY = 0
            if (paddleX < 0)
                paddleX = 0
            if ((paddleY + rect.height) >= imgHeight)
                paddleY = (imgHeight - rect.height) - 1
            if ((paddleX + rect.width) >= imgWidth)
                paddleX = (imgWidth - rect.width) - 1

            label.setTranslateY(paddleY)
            label.setTranslateX(paddleX)
            rect.setY(paddleY)
            rect.setX(paddleX)
        }
    }

    private EventHandler<KeyEvent> keyReleased = new EventHandler<KeyEvent>() {
        @Override
        void handle(KeyEvent keyEvent) {
            switch (keyEvent.code) {
                case KeyCode.W:
                case KeyCode.S:
                    paddleDY = 0
                    break
                case KeyCode.A:
                case KeyCode.D:
                    paddleDX = 0
                    break
                case KeyCode.Q:
                    if (label.isVisible())
                        label.setVisible(false)
                    else
                        label.setVisible(true)
            }
        }
    }

    private EventHandler<KeyEvent> keyPressed = new EventHandler<KeyEvent>() {
        @Override
        void handle(KeyEvent keyEvent) {
            switch (keyEvent.code) {
                case KeyCode.W:
                    paddleDY = keyEvent.shiftDown ? -5 : -0.5d
                    break
                case KeyCode.S:
                    paddleDY = keyEvent.shiftDown ? 5 : 0.5d
                    break
                case KeyCode.A:
                    paddleDX = keyEvent.shiftDown ? -5 : -0.5d
                    break
                case KeyCode.D:
                    paddleDX = keyEvent.shiftDown ? 5 : 0.5d
                    break
            }
        }
    }
}
