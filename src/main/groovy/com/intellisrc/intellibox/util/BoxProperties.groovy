package com.intellisrc.intellibox.util

import groovy.transform.CompileStatic

@CompileStatic
class BoxProperties {
    String name
    double height
    double width

    void setName(String name) {
        this.name = name
    }

    void setHeight(double height) {
        this.height = height
    }

    void setWidth(double width) {
        this.width = width
    }

    String getName() {
        return name
    }

    double getHeight() {
        return height
    }

    double getWidth() {
        return width
    }
}
