package com.intellisrc.intellibox.util

import com.intellisrc.core.Log
import com.intellisrc.intellibox.format.Formats
import groovy.transform.CompileStatic

/**
 * This class will process all the images that has been annotated and will
 * produce train.txt and test.txt
 * @since 18/09/05.
 */
@CompileStatic
class Process {
    // strings
    //private final String processDir = Config.get("process.dir") ?: "Annotations"

    // ints
    private int percentage

    // lists
    List<String> imgNames = []

    void startProcessing(String imagesDir, String annotationsDir, String train, String test, int percent) {
        File tempImagesDir = new File(imagesDir)
        File tempSubsetDir = new File(annotationsDir)

        assert tempImagesDir.exists() : "Directory doesn't exits" //If UI is outside this class, wrap with try {} catch (AssertException)
        if (tempImagesDir.exists()) {
            tempImagesDir.eachFile {
                File file ->
                    if (!file.isDirectory()) {
                        if (file =~ /(?i)\.(jpe?g|png|gif|tiff)$/) { //TODO: test with ImageIO
                            if(Formats.getAnnotation(file)) {
                                imgNames << file.absolutePath
                            }
                        }
                    }
            }
        } else {
            //TODO: display error in screen
            Log.e("Directory doesn't exists: ${tempImagesDir.absolutePath}")
            return
        }

        File trainFile = new File(tempSubsetDir.path + File.separator + train)
        trainFile.text = ""
        File testFile = new File(tempSubsetDir.path + File.separator + test)
        testFile.text = ""
        percentage = Math.round(imgNames.size() / percent as float) as int

        Collections.shuffle(imgNames)

        imgNames.subList(0, percentage).each {
            testFile << it + "\n"
        }
        imgNames.subList(percentage, imgNames.size()).each {
            trainFile << it + "\n"
        }
    }
}
