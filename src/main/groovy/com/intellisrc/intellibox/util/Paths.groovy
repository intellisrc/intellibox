package com.intellisrc.intellibox.util

import com.intellisrc.core.Config
import groovy.transform.CompileStatic

@CompileStatic
final class Paths {
    static final String RESOURCE_BUNDLE = "bundle" + File.separator +"configSrc" // bundle/configSrc
    static final String RESOURCE_PATH = "resources" + File.separator             // resources/
    static final String ICON_PATH = RESOURCE_PATH + "icons" + File.separator     // /resources/icons/
    static final String FXML_PATH = RESOURCE_PATH + "fxml" + File.separator      // /resources/fxml/
    static final String DIALOG_PATH = File.separator + "fxml" + File.separator
    static private File imageDirectory
    static void setImageDirectory(File newDir) {
        imageDirectory = newDir
    }
    static File getImageDirectory() {
        if(!imageDirectory && Config.hasKey("images.dir")) {
            imageDirectory = new File(Config.get("images.dir"))
        }
        if(!imageDirectory) {
            imageDirectory = new File(".")
        }
        return imageDirectory
    }
    private Paths() {}
}
