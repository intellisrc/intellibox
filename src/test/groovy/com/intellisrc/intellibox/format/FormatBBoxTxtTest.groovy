package com.intellisrc.intellibox.format

import com.intellisrc.core.SysInfo
import com.intellisrc.intellibox.util.Annotation
import com.intellisrc.intellibox.util.Class
import javafx.scene.shape.Rectangle
import spock.lang.Specification

/**
 * @since 18/09/05.
 */
class FormatBBoxTxtTest extends Specification {
    static final String testDir = SysInfo.userDir + "resources/test/"
    static final String testImg = testDir + "city.jpg"
    static final String testImgColors = testDir + "colors.jpg"

    Annotation a = null
    def format = new FormatBBoxTxt()

    def "Import BBox"() {
        setup:
        a = format.getAnnotation(new File(testImg))

        expect:
        assert a.image
        assert a.boxes.size() == 22
        assert Class.list.size() == 4
    }

    def "Export BBox"() {
        setup:
        a = new Annotation(new File(testImgColors))
        File out = format.getExportFile(a)
        Class.addClass("color")
        a.boxes << new Annotation.Box(
                cls: Class.fromId(0),
                rect: new Rectangle(50, 75, 100, 120)
        )

        expect:
        assert format.export(a)
        assert out.exists()

        when:
        Annotation aa = format.getAnnotation(new File(testImgColors))

        then:
        assert a == aa

        cleanup:
        out.delete()
    }
}