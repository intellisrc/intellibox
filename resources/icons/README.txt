Silk icon set 1.3
----------------------------------------
Mark James
http://www.famfamfam.com/lab/icons/silk/
----------------------------------------

“Silk” is a smooth, free icon set, containing over 700 16-by-16 pixel icons in strokably-soft PNG format. Containing a large variety of icons, you're sure to find something that tickles your fancy. 

Licence

The icons are dual licensed under Creative Commons Attribution 2.5 License and Creative Commons Attribution 3.0 License.

This means you may use it for any purpose,
and make any changes you like.
All I ask is that you include a link back
to this page in your credits.

Are you using this icon set? Send me an email
(including a link or picture if available) to
mjames@gmail.com

Any other questions about this icon set please
contact mjames@gmail.com
